'use strict';
import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  View,
  TouchableHighlight
} from 'react-native';


var MOCKED_QUESTIONS = [
  { question: "Ile masz stóp?", correctAnswer: 1, answers: ["jeden", "dwa", "trzy"] },
  { question: "Czy cośtam?", correctAnswer: 1, answers: ["nigdy", "czasem", "zawsze"] },
  { question: "Co znajduje się na okładce Hobbita?", correctAnswer: 1, answers: ["klucz", "hobbit", "śrubokręt"] },
  { question: "Jak zdać egzamin z BSI?", correctAnswer: 1, answers: ["fartem", "ziemniak", "kanapka"] }
];


class Quiz extends Component {
  constructor() {
    super();

    let initialQuestionIndex = this.getRandomQuestionIndex();

    this.state = {
      question: MOCKED_QUESTIONS[initialQuestionIndex],
      selectedAnswerIndex: null,
      answeredQuestions: [initialQuestionIndex]
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.label}>Pytanie:</Text>
        <View style={styles.questionBox}>
          <Text style={styles.whiteText}>{this.state.question.question}</Text>
        </View>
        <Text style={styles.label}>Odpowiedzi:</Text>
        {this.state.question.answers.map(this.createAnswer.bind(this))}
        {this.nextQuestionButton()}
      </View>
    );
  }

  nextQuestionButton() {
    if (this.state.selectedAnswerIndex === null) {
      return null;
    } else {
      return <TouchableHighlight quiz={this} style={{marginTop: 30}} onPress={this.nextQuestion}>
        <View style={styles.nextQuestion}>
          <Text style={styles.whiteText}>Następne pytanie</Text>
        </View>
      </TouchableHighlight>
    }
  }

  nextQuestion() {
    this.quiz.setState({
      selectedAnswerIndex: null,
      question: this.quiz.getRandomQuestion()
    });
  }

  getRandomQuestion() {
    // Clear answered questions array if all were answered
    if (this.state.answeredQuestions.length == MOCKED_QUESTIONS.length) {
      this.setState({ answeredQuestions: [] });
    }

    // Find a new question index excluding the ones answered
    let questionIndex = this.getRandomQuestionIndex();
    while (this.state.answeredQuestions.some(q => q == questionIndex)) {
      questionIndex = this.getRandomQuestionIndex();
    }

    // Add the new index to answered questions list
    let answeredQuestions = this.state.answeredQuestions;
    answeredQuestions.push(questionIndex);

    this.setState({
      answeredQuestions: answeredQuestions
    });

    return MOCKED_QUESTIONS[questionIndex];
  }

  getRandomQuestionIndex() {
    return Math.round(Math.random() * (MOCKED_QUESTIONS.length - 1));
  }

  highlightAnswer() {
    if (this.quiz.state.selectedAnswerIndex === null) {
      this.quiz.setState({ selectedAnswerIndex: this.answerIndex });
    }
  }

  createAnswer(answer, index) {
    var answerStyle = styles.answer;
    if (this.state.selectedAnswerIndex === index) {
      if (this.state.selectedAnswerIndex === this.state.question.correctAnswer) {
        answerStyle = styles.answerCorrect;
      } else {
        answerStyle = styles.answerIncorrect;
      }
    } else if (this.state.selectedAnswerIndex !== null) {
      if (this.state.question.correctAnswer == index) {
        answerStyle = styles.answerCorrect;
      }
    }
    return <TouchableHighlight key={index} quiz={this} answerIndex={index} onPress={this.highlightAnswer}>
      <View style={answerStyle}>
        <Text style={styles.whiteText}>{answer}</Text>
      </View>
    </TouchableHighlight>;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
    flexDirection: "column"
  },
  label: {
    marginTop: 30,
    fontSize: 10,
    padding: 5
  },
  questionBox: {
    padding: 10,
    backgroundColor: "#E84F6C"
  },
  whiteText: {
    fontSize: 20,
    textAlign: "center",
    color: "white"
  },
  nextQuestion: {
    padding: 10,
    backgroundColor: "#DDDDDD"
  },
  answer: {
    padding: 10,
    backgroundColor: "#4FE8E5",
    borderBottomColor: "white",
    borderBottomWidth: 1
  },
  answerCorrect: {
    padding: 10,
    backgroundColor: "#00ff00",
    borderBottomColor: "white",
    borderBottomWidth: 1
  },
  answerIncorrect: {
    padding: 10,
    backgroundColor: "#ff0000",
    borderBottomColor: "white",
    borderBottomWidth: 1
  }
});

export default Quiz
