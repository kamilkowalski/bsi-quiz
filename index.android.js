/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';
import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  View
} from 'react-native';

import Quiz from './quiz';


class BsiQuiz extends Component {
  render() {
    return <Quiz/>;
  }
}

AppRegistry.registerComponent('BsiQuiz', () => BsiQuiz);
